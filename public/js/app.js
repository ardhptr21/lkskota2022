// NAVBAR TOGGLER
const navbarIcon = document.querySelector('.navbar-icon img');
const navbarLink = document.querySelector('.navbar-link');

if (navbarIcon) {
    navbarIcon.addEventListener('click', () => {
        navbarLink.classList.toggle('show');
        navbarIcon.src = navbarIcon.src.includes('menu') ? '/images/ic-close.png' : '/images/ic-menu.png';
    })
}

// SECTION ANIMATION
ScrollReveal().reveal('.hero', { delay: 500, duration: 2000 })
ScrollReveal().reveal('.about', { duration: 2000, viewOffset: { bottom: 400 } })
ScrollReveal().reveal('.blog', { duration: 2000, viewOffset: { bottom: 400 } })

// COMPONENT ANIMATION
ScrollReveal().reveal('.blog-card', { duration: 1000, interval: 500, viewOffset: { bottom: 400, } })
ScrollReveal().reveal('.contact .section-title', { duration: 2000, viewOffset: { bottom: 400 } })
ScrollReveal().reveal('.contact-media', { delay: 1000, duration: 1000, interval: 500, viewOffset: { bottom: 200 } })
