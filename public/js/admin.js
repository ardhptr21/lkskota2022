// IMAGE THUMBNAIL INPUT PREVIEW
const formInputThumbnail = document.querySelector('.form-input-thumbnail');
const formThumbnail = document.querySelector('.form-thumbnail');

if (formInputThumbnail) {
    formInputThumbnail.addEventListener('change', (e) => {
        const file = e.target.files[0];
        const blob = URL.createObjectURL(file)
        formThumbnail.src = blob;
        formThumbnail.style = 'display: block';
    })
}
