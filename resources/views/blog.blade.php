@extends('layouts.base', ['title' => $blog->title])

@section('content')
    <section class="blog-page">
        <div class="blog-meta">
            <img class="blog-img" src="{{ '/' . $blog->image }}" alt="{{ $blog->title }}">
            <div class="">
                <h1 class="blog-title">{{ $blog->title }}</h1>
                <div class="blog-meta-info">
                    <h5 class="blog-category">{{ $blog->category->name }}</h5>
                    <h6 class="blog-date">{{ \Carbon\Carbon::parse($blog->created_at)->format('d M Y') }}</h6>
                </div>
            </div>
        </div>
        <p class="blog-content">{{ $blog->content }}</p>
    </section>
@endsection
