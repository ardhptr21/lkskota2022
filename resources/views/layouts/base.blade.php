<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <title>{{ $title }} | brand.co</title>
    @include('partials.head')
</head>

<body>
    <header>
        @include('partials.navbar')
    </header>

    <main id="home">
        @yield('content')
    </main>

    @include('partials.footer')
    @include('partials.tail')
</body>

</html>
