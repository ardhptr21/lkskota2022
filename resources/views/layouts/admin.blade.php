<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <title>Admin | {{ $title }}</title>
    @include('partials.head')
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
</head>

<body>
    @if (!isset($withoutNavbar) || !$withoutNavbar)
        <header>
            @include('partials.navbar')
        </header>
    @endif

    <main class="admin">
        @yield('content')
    </main>

    <script src="{{ asset('js/admin.js') }}"></script>
</body>

</html>
