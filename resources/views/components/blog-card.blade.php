<a href="{{ $slug != '' ? route('blog', $slug) : '#' }}" class="blog-card {{ $class }}">
    <img class="blog-card-img" src="{{ $image }}" alt="{{ $title }}">
    <div class="blog-card-content">
        <small class="blog-card-date">{{ $date }}</small>
        <h3 class="blog-card-title">{{ $title }}</h3>
        <p class="blog-card-short">{{ $short }}</p>
    </div>
</a>
