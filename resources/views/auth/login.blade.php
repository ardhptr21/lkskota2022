@extends('layouts.admin', ['title' => 'Login', 'withoutNavbar' => true])

@section('content')
    <section class="login">
        <form action="" method="POST" class="login-form">
            <h1 class="section-title" style="text-align: left">Login</h1>
            @csrf
            <div class="login-fields">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" value="{{ old('email') }}" placeholder="Input email"
                        class="form-input" required>
                    @error('email')
                        <div class="form-invalid">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" placeholder="Input password" class="form-input" required>
                    @error('password')
                        <div class="form-invalid">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button class="btn btn-primary" type="submit">Submit</button>
                @if (session('error'))
                    <div class="feedback invalid">
                        {{ session('error') }}
                    </div>
                @endif
            </div>
        </form>
    </section>
@endsection
