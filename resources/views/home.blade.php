@extends('layouts.base', ['title' => 'Home'])

@section('content')
    <section class="hero">
        <h1 class="hero-title">
            <span class="hero-title-header">Welcome to brand.co</span>
            <span><br> Building digital <br>products & brands</span>
        </h1>
        <img class="hero-image" src="{{ asset('images/hero/img-people.png') }}" alt="img-people">
    </section>
    <section class="about" id="about">
        <h2 class="section-title">About Us</h2>
        <p class="about-text">We are Digital Product Designer and Creative Designer in Dhaka.
            Specialize in UX/UI Design, Responsive Web & Mobile Design,
            Front-end Development, and more</p>
    </section>
    <section class="blog" id="blog">
        <h2 class="section-title">Blogs</h2>
        <div class="blog-list">
            <div class="blog-list-head">
                <x-blog-card image="{{ asset('images/blog/img-blog-2.png') }}"
                    title="UX Mentorship: How to Get Ahead in Your Career in 2022" date="12 Jan 2022 · Announcement"
                    short="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. " />
                <div class="blog-list-side">
                    <x-blog-card image="{{ asset('images/blog/img-blog-1.png') }}"
                        title="The key to clean user interface design" date="12 Jan 2022 · Announcement"
                        short="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. " />
                    <x-blog-card image="{{ asset('images/blog/img-blog-3.png') }}"
                        title="Analyzing Apple Music’s User Experience" date="12 Jan 2022 · Announcement"
                        short="Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. " />
                </div>
            </div>
            <div class="blog-list-all">
                @foreach ($blogs as $blog)
                    <x-blog-card :image="$blog->image" :title="$blog->title" :slug="$blog->slug"
                        date="{{ \Carbon\Carbon::parse($blog->created_at)->format('d M Y') }} · {{ $blog->category->name }}"
                        short="{{ substr($blog->content, 0, 100) }}..." />
                @endforeach
            </div>
        </div>
    </section>
    <section class="contact" id="contact">
        <h2 class="section-title">Contact Us</h2>
        <div class="contact-list">
            <div class="contact-media">
                <img class="media-img" src="{{ asset('images/media/ic-logo-fb-big.png') }}" alt="facebook">
                <h5 class="media-handle">@brand.co</h5>
                <h6 class="media-name">Facebook</h6>
            </div>
            <div class="contact-media">
                <img class="media-img" src="{{ asset('images/media/ic-logo-twitter-big.png') }}" alt="twitter">
                <h5 class="media-handle">@brand.co</h5>
                <h6 class="media-name">Twitter</h6>
            </div>
            <div class="contact-media">
                <img class="media-img" src="{{ asset('images/media/ic-logo-wa-big.png') }}" alt="twitter">
                <h5 class="media-handle">0812 3456 789</h5>
                <h6 class="media-name">Whatsapp</h6>
            </div>
        </div>
    </section>
@endsection
