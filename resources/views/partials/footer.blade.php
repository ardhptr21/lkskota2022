<footer class="footer">
    <div class="footer-content">
        <img class="footer-logo" src="{{ asset('images/img-logo-white.png') }}" alt="logo">

        <div class="footer-link">
            <a href="/">Home</a>
            <a href="#about">About Us</a>
            <a href="#blog">Blog</a>
            <a href="#contact">Contact Us</a>
        </div>

        <div class="footer-media">
            <img src="{{ asset('images/media/ic-logo-fb-small.png') }}" alt="facebook">
            <img src="{{ asset('images/media/ic-logo-twitter-small.png') }}" alt="twitter">
            <img src="{{ asset('images/media/ic-logo-wa-small.png') }}" alt="whatsapp">
        </div>
    </div>

    <p class="footer-copyright">Copyright Ardhi Putra ©2022. All rights reserved </p>
</footer>
