<nav class="navbar">
    <a href="/" class="navbar-logo"><img src="{{ asset('images/img-logo.png') }}" alt="logo"></a>
    <div class="navbar-icon">
        <img src="{{ asset('images/ic-menu.png') }}" alt="icon-menu">
    </div>
    <ul class="navbar-link">
        <li class="navbar-item {{ request()->is('/') ? 'active' : '' }}"><a href="/">Home</a></li>
        @if (request()->is('admin*'))
            <li class="navbar-item {{ request()->is('admin') ? 'active' : '' }}">
                <a href="{{ route('admin') }}">Dashboard</a>
            </li>
            <li class="navbar-item {{ request()->is('admin/blog*') ? 'active' : '' }}">
                <a href="{{ route('admin.blog') }}">Blog</a>
            </li>
            <form action="{{ route('logout') }}" method="POST">
                @csrf
                <input type="submit" hidden id="logout">
                <li class="navbar-item">
                    <a href=""><label for="logout" style="cursor: pointer;">Logout</label></a>
                </li>
            </form>
        @else
            <li class="navbar-item"><a href="#about">About Us</a></li>
            <li class="navbar-item"><a href="#blog">Blog</a></li>
            <li class="navbar-item"><a href="#contact">Contact Us</a></li>
            <li class="navbar-item {{ request()->is('admin') ? 'active' : '' }}">
                <a href="{{ route('admin') }}">Dashboard</a>
            </li>
        @endif
    </ul>
</nav>
