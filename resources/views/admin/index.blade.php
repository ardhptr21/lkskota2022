@extends('layouts.admin', ['title' => 'Selamat Datang'])

@section('content')
    <h1 class="admin-supertitle" style="text-align: left">Welcome, Admin!</h1>
    <p class="admin-text">Selamat datang di halaman admin! Kami sangat senang bisa menyambut Anda di sini. Halaman ini adalah
        pusat kendali untuk semua hal yang berkaitan dengan situs, dan kami berharap bisa bekerja sama dengan Anda untuk
        menjaga situs tetap optimal. Kami siap membantu dalam mengelola situs dengan baik, termasuk mengecek aktivitas
        terbaru, permintaan masuk, dan semua hal penting lainnya. Admin yang sukses adalah yang mampu mengelola semua hal
        dengan baik, dan itulah yang kami harapkan dari setiap admin di sini. Oleh karena itu, mari kita mulai hari ini
        dengan semangat baru dan menghadapi semua tantangan dengan kepala tegak. Kami siap mendukung setiap keputusan yang
        diambil demi kebaikan situs, dan semoga harimu menyenangkan!
    </p>
    <p class="admin-text">
        Sebagai admin, Anda memiliki tanggung jawab besar dalam menjaga situs tetap terorganisir dan berjalan lancar. Oleh
        karena itu, halaman ini menjadi sangat penting sebagai tempat dimana semua kontrol terpusat. Kami akan senantiasa
        siap membantu dan mendukung Anda dalam setiap tugas dan tanggung jawab yang diemban. Ayo, mari kita mulai hari ini
        dengan semangat dan tekad yang kuat untuk mengelola situs ini dengan baik, demi kebaikan semua pengguna dan
        komunitas yang ada di dalamnya. Terima kasih telah bergabung dengan kami di halaman admin, dan semoga kita bisa
        bekerja sama dengan baik untuk mencapai tujuan bersama. Selamat bekerja dan selamat mengelola website ini dengan
        baik dan penuh dengan tanggung jawab, agar situs ini tetap terus berkembang dan menjadi lebih baik lagi!
    </p>
@endsection
