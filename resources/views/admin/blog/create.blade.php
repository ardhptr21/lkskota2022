@extends('layouts.admin', ['title' => 'Create Blog'])

@section('content')
    <h1 class="admin-title">Blog</h1>
    <div class="blog-content">
        <form action="" method="POST" class="form-blog" enctype="multipart/form-data">
            @csrf
            <h2 class="admin-subtitle">Create Blog</h2>
            <div class="form-group">
                <label for="image">Image</label>
                <input type="file" name="image" class="form-input-thumbnail" required>
                @error('image')
                    <div class="form-invalid">{{ $message }}</div>
                @enderror
                <img src="" class="form-thumbnail" alt="preview" style="display: none">
            </div>
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" value="{{ old('title') }}" name="title" class="form-input" placeholder="Input text"
                    required>
                @error('title')
                    <div class="form-invalid">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="category">Category</label>
                <select name="category" id="category" class="form-input" required>
                    <option value="" hidden>Select Category</option>
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="content">Content</label>
                <textarea name="content" id="content" class="form-input" cols="30" rows="10" required>{{ old('content') }}</textarea>
                @error('content')
                    <div class="form-invalid">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-blog-action">
                <a href="{{ route('admin.blog') }}" class="btn btn-outline btn-primary">Cancel</a>
                <button class="btn btn-primary" type="submit">Save</button>
            </div>
            @if (session('error'))
                <div class="feedback invalid">{{ session('error') }}</div>
            @endif
        </form>
    </div>
@endsection
