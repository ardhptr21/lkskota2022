@extends('layouts.admin', ['title' => 'Blog'])

@section('content')
    <h1 class="admin-title">Blog</h1>
    <a href="{{ route('admin.blog.create') }}"><button class="btn btn-primary" style="margin-bottom: 10px">Add</button></a>
    <div class="blog-content">
        @if (session('success'))
            <div class="feedback valid" style="margin-bottom: 1rem">
                {{ session('success') }}
            </div>
        @endif
        <h2 class="admin-subtitle">List</h2>
        <div class="blog-table">
            <table class="table">
                <thead class="thead">
                    <tr>
                        <th>No</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Content</th>
                        <th>Date Created</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($blogs as $blog)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>
                                <img src="{{ '/' . $blog->image }}" alt="{{ $blog->title }}" class="img-thumbnail">
                            </td>
                            <td>{{ $blog->title }}</td>
                            <td>{{ $blog->category->name }}</td>
                            <td>{{ $blog->content }}</td>
                            <td>{{ \Carbon\Carbon::parse($blog->created_at)->format('d M Y') }}</td>
                            <td>
                                <div class="form-blog-action">
                                    <a href="{{ route('admin.blog.edit', $blog->id) }}"
                                        class="btn btn-primary btn-outline">Edit</a>
                                    <form action="{{ route('admin.blog.destroy', $blog->id) }}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-primary">Delete</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
