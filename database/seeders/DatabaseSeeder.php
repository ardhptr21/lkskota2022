<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'admin@email.com',
            'name' => 'Admin',
            'password' => bcrypt('admin'),
        ]);

        $this->call([
            CategorySeeder::class
        ]);
    }
}
