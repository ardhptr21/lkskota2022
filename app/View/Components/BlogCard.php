<?php

namespace App\View\Components;

use Illuminate\View\Component;

class BlogCard extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(public $image, public $date, public $title, public $short, public $class = '', public string $slug = '')
    {
        $this->image = $image;
        $this->date = $date;
        $this->title = $title;
        $this->short = $short;
        $this->class = $class;
        $this->slug = $slug;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.blog-card');
    }
}
