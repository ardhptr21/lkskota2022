<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home()
    {
        $blogs = Blog::with('category:id,name')->get();
        return view('home', compact('blogs'));
    }

    public function blog(Blog $blog)
    {
        return view('blog', compact('blog'));
    }
}
