<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{
    public function index()
    {
        $blogs = Blog::with('category:id,name')->get();
        return view('admin.blog.index', compact('blogs'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('admin.blog.create', compact('categories'));
    }

    public function edit(Blog $blog)
    {
        $categories = Category::all();
        return view('admin.blog.edit', compact('blog', 'categories'));
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required',
            'content' => 'required',
            'category' => 'required',
            'image' => 'required|file',
        ]);

        $validated['category_id'] = $validated['category'];

        $image = $request->file('image')->store('public/uploads');
        if (!$image) return back()->withErrors(['error' => 'Unggah gambar gagal!']);
        $validated['image'] = str_replace('public', 'storage', $image);

        $slug = str_replace(' ', '-', strtolower($validated['title']));

        $count = Blog::where('slug', 'like', $slug . '%')->count();
        if ($count > 0) {
            $slug = $slug . '-' . $count;
        }

        $validated['slug'] = $slug;

        $blog = Blog::create($validated);

        if (!$blog) {
            Storage::delete(str_replace('storage', 'public', $image));
            return back()->with('error', 'Gagal membuat blog!');
        }

        return to_route('admin.blog')->with('success', 'Berhasil membuat blog!');
    }

    public function update(Request $request, Blog $blog)
    {
        $validated = $request->validate([
            'title' => 'required',
            'content' => 'required',
            'category' => 'required',
            'image' => 'file',
        ]);

        $validated['category_id'] = $validated['category'];

        if ($request->hasFile('image')) {
            $image = $request->file('image')->store('public/uploads');
            if (!$image) return back()->withErrors(['error' => 'Unggah gambar gagal!']);
            $validated['image'] = str_replace('public', 'storage', $image);
            Storage::delete(str_replace('storage', 'public', $blog->image));
        }

        $blog->update($validated);

        if (!$blog) {
            Storage::delete(str_replace('storage', 'public', $image));
            return back()->with('error', 'Gagal mengedit blog!');
        }

        return back()->with('success', 'Berhasil mengedit blog!');
    }

    public function destroy(Blog $blog)
    {
        $image = $blog->image;
        $blog->delete();
        Storage::delete(str_replace('storage', 'public', $image));
        return back()->with('success', 'Berhasil menghapus blog');
    }
}
