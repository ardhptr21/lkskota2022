<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\PagesController;
use Illuminate\Support\Facades\Route;

Route::controller(PagesController::class)->group(function () {
    Route::get('/', 'home')->name('home');
    Route::get('/blog/{blog:slug}', 'blog')->name('blog');
});

Route::prefix('/auth')->controller(AuthController::class)->group(function () {
    Route::get('/login', 'login')->name('login');
    Route::post('/login', 'authenticated');
    Route::post('/logout', 'logout')->name('logout');
});

Route::view('/admin', 'admin.index')->name('admin')->middleware('auth');
Route::prefix('/admin/blog')->controller(BlogController::class)->middleware('auth')->group(function () {
    Route::get('/{blog}/edit', 'edit')->name('admin.blog.edit');
    Route::get('/create', 'create')->name('admin.blog.create');
    Route::get('/', 'index')->name('admin.blog');

    Route::put('/{blog}/edit', 'update');
    Route::post('/create', 'store');
    Route::delete('/{blog}', 'destroy')->name('admin.blog.destroy');
});
